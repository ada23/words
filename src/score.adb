with Ada.Text_Io ; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Long_Long_Integer_Text_IO ; use Ada.Long_Long_Integer_Text_IO ;
with Ada.Command_Line ; use Ada.Command_Line ;

with cli ;
with words ;
with letters ;
with scrabble ;

procedure score is
   dict : words.Dictionary ;
begin
   words.Load(Argument(1),dict);
   scrabble.Initialize ;
   loop
      declare
         ans : String := cli.Get("Word");
      begin
         if ans = "quit"
         then
            return ;
         end if ;
         if words.IsWord(ans,dict)
         then
            Put("Score ");
            Put(scrabble.Score(ans));
            Put(" Signature ");
            Put(Long_Long_Integer(letters.Signature(ans,true)));
         else
            Put("It is not a word");
         end if ;
         New_Line ;
      end ;
   end loop ;
end score;
