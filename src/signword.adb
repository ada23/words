with Text_Io ; use Text_Io ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Long_Long_Integer_Text_IO ; use Ada.Long_Long_Integer_Text_IO ;
with Ada.Command_Line ;
with Interfaces ; use Interfaces ;

with letters ;
procedure signword is

   
   wordfile : Text_Io.File_Type ;
   lineword : String(1..64);
   linewordlen : Natural ;
   summary : array (1..1000) of integer := (others => 0) ;
   procedure Signword( w : string ) is
      s : Interfaces.Unsigned_64 ;
   begin
      s := letters.Signature(w) ;
      Put(w) ;
      Set_Col(24) ;
      Put(Long_Long_Integer(s)) ; 
      New_Line ;
      summary(Integer(s)) := summary(Integer(s)) + 1;
   exception
      when others => null;
   end Signword ;
   
begin
   if Ada.Command_Line.Argument_Count < 1
   then   
      Signword("SIGNATURE");
      Signword("QUIET");
      Signword("QUITE");
      Signword("BLOID");
      return ;
   end if ;
   Open( wordfile, In_File, Ada.Command_Line.Argument(1) ) ;
   while not End_Of_File(wordfile)
   loop
      Get_Line(wordfile,lineword,linewordlen);
      Signword(lineword(1..linewordlen));
   end loop ;
   Close(wordfile) ;
   for sidx in summary'range
   loop
      Put(sidx);
      Set_Col(15);
      Put(summary(sidx));
      New_line ;
   end loop ;
   
end signword;
