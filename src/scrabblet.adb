with Text_Io; use Text_Io;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
WITH GNAT.Strings ;

with cli ; use cli ;
with scrabble.dealer ;
with scrabble.player ;

with words ;

procedure scrabblet is
   dict : words.Dictionary ;
   procedure Dealer_Mode is
      cmd : cli.Commands := cli.undefined ;
      userguess : unbounded_string ;
   begin
      while cmd /= cli.quit
      loop
         Cli.Get("Dealer" , cmd , userguess);
      end loop ;
   end Dealer_Mode ;
   
   procedure Player_MOde is
      cmd : cli.Commands := cli.undefined ;
      usersaid : Unbounded_String ;
   begin
      while cmd /= cli.quit
      loop
         cli.get("Player" , cmd , usersaid);
      end loop ;
   end Player_Mode ;
   
begin   
   cli.ProcessCommandLine;

   if cli.verbosity > 0
   then
      words.verbose := true ;
   end if ;
   if cli.dictfile.all'length < 1
   then
      Put_Line("Need a dictionary file");
      return ;
   end if ;
   words.load( cli.dictfile.all , dict ) ;
   case cli.command is
      when cli.dealer =>
         Put_Line("Dealer Mode");
         scrabble.dealer.Play (dict) ;
      when cli.Player =>
         Put_Line("Player mode");
         declare
            wc : string := cli.GetNextArgument ;
            w : string := cli.GetNextArgument ;
         begin
            if w'length >= 3
            then
               scrabble.Player.Play(dict,w);
            else
               scrabble.player.Play(dict) ;
            end if ;
         end ;
      when others =>
         Put_Line("Unsupported command ");
   end case ;

end scrabblet;
