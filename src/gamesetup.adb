with Text_Io; use Text_Io;
with scrabble ;
with scrabble.dealer ;

procedure gamesetup is
   b : scrabble.bagptr ;
begin
   scrabble.Initialize ;
   b := scrabble.Create ;
   scrabble.Show(b);
   for i in 1..10
   loop
      Put_Line( scrabble.dealer.Deal(b) ) ;
   end loop ;
   --scrabble.dealer.Play ;
end gamesetup;
