with Ada.Command_Line ; use Ada.Command_Line ;
with Ada.Text_Io; use Ada.Text_Io;

with words;
procedure dict is
   c : words.Dictionary ;
begin
   words.Load(Ada.Command_Line.Argument(1),c);
   words.Show(c) ;
   for idx in 2..Ada.Command_Line.Argument_Count
   loop
      Put( Argument(idx) ) ; Put(" ");
      Put(boolean'Image(words.IsWord(Argument(idx),c))); Put(" ");
      Put(boolean'Image(words.IsPossibleWord(Argument(idx),c)));
      New_Line ;
   end loop ;
end dict;
