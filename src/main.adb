with Text_Io; Use Text_Io ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Command_Line; use Ada.Command_Line ;
with letters;
with words ;

procedure Main is
   e : words.Enumerator ;
   d : words.Dictionary ;
   pw : Integer := 0 ;
   wn : Integer := 0 ;
begin
   if argument_count > 0
   then
      words.Load(argument(1),d) ;
   end if;

   --words.ListAnagrams(d,"LABEL") ;
   --Put_Line("-----------------");
   --e := words.Create("ABCDEFG");
   e := words.Create("ABLETEX");
   while Not words.NoMoreWords(e)
   loop
      declare
         nw : string := words.Next(e) ;
      begin
         Put("Sub word "); Put(nw); Put_LIne(" ----------------------");
         if argument_count > 0
         then
            if words.IsPossibleWord(nw,d)
            then

               pw := pw + 1 ;
               Put(wn); Put(" is a possible word. Signature: ");
               Put(Integer(letters.Signature(nw))); Put(" ");
               Put_Line( nw ) ;
            end if ;
         end if ;
         wn := wn+1 ;
      end ;
   end loop;
   Put("Potential words "); Put(Integer'Image(pw)); New_Line ;
end Main;
