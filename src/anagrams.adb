with Ada.Command_Line; use Ada.Command_Line ;

with words ;

procedure anagrams is
   dict : words.Dictionary ;
begin
   if Argument_Count > 0
   then
      words.Load(Argument(1),dict);
   end if ;
   if Argument_Count > 1
   then
      for arg in 2..Argument_Count
      loop
         words.ListAnagrams(dict,Argument(arg));
      end loop ;
   end if ;
end anagrams;
