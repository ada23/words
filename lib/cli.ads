with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with GNAT.Strings ;
package cli is

   VERSION : string := "V01" ;
   NAME : String := "scrabblet" ;


   type commands is
     (
      undefined ,
      dealer ,
      player ,
      quit ,
      hand ,
      hint ,
      score ,
      guess ,
      show ,
      help ) ;

   command : commands := undefined ;
   Verbosity : aliased Integer := 0 ;

   dictfile : aliased GNAT.Strings.String_Access ;

   procedure ProcessCommandLine ;
   function GetNextArgument (expand : boolean := false ) return String ;

   function Get(prompt : string) return String ;

   --function Get(prompt : string) return Commands ;
   procedure Get(prompt : string ; cmd : out commands ; resp : out Unbounded_String) ;
end cli;
