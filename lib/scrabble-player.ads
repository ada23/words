with words ;
package scrabble.player is

   procedure Play (dict : words.Dictionary) ;
   procedure Play (dict : words.Dictionary ; hand : String) ;

end scrabble.player;
