with Ada.Text_Io; use Ada.Text_Io;
with Ada.Strings.Fixed ; use Ada.Strings.Fixed ;

with GNAT.Command_Line ;
with GNAT.Source_Info; use GNAT.Source_Info;

package body cli is

   procedure SwitchHandler
     (Switch : String; Parameter : String; Section : String)
   is
   begin
      Put ("SwitchHandler " & Switch);
      Put (" Parameter " & Parameter);
      Put (" Section " & Section);
      New_Line;
   end SwitchHandler;

   procedure ProcessCommandLine is
      Config : GNAT.Command_Line.Command_Line_Configuration;
   begin

      GNAT.Command_Line.Set_Usage
        (Config,
         Help =>
           NAME & " " & VERSION & " " & Compilation_ISO_Date & " " &
           Compilation_Time,
         Usage => "subcommands: dealer|player ...  .use --help with the subcommand");

      GNAT.Command_Line.Initialize_Option_Scan(Stop_At_First_Non_Switch => true );
      GNAT.Command_Line.Define_Switch
        (Config, Verbosity'access, Switch => "-v:", Long_Switch => "--verbosity:",
         Help                           => "Verbosity Level");
      GNAT.Command_Line.Define_Switch
        (Config, dictfile'access, Switch => "-d:", Long_Switch => "--dictionary:",
         Help                           => "dictionary file to load");

      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);
      declare
         subcommand : string := GNAT.Command_Line.Get_Argument (Do_Expansion => False);
      begin
         if subcommand = "dealer"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "dealer mode"
                                        );
            command := dealer ;

         elsif subcommand = "player"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "player mode"
                                        );
            command := player ;

         elsif subcommand = "help"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "show predefined ids"
                                        );
            command := help ;
            Put_Line("Help command");
         end if ;
      end ;

      GNAT.Command_Line.Initialize_Option_Scan(Stop_At_First_Non_Switch => false );
      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);

   end ProcessCommandLine;

   function GetNextArgument (expand : boolean := false )  return String is
   begin
      return GNAT.Command_Line.Get_Argument (Do_Expansion => expand);
   end GetNextArgument;


   function Get(Prompt : string) return string is
      result : String(1..80);
      len : natural ;
   begin
      Put(Prompt) ; Put(" > "); Flush ;
      Get_Line(result,len);
      return result(1..len);
   end Get ;
   procedure Get(prompt : string ; cmd : out commands ; resp : out Unbounded_String ) is
      response : string := Get(prompt) ;
   begin
      resp := Null_Unbounded_String ;
      if response = "quit"
      then
         cmd := quit ;
         return ;
      end if;
      if response = "hint"
      then
         cmd := hint ;
         return ;
      end if ;
      if response = "hand"
      then
         cmd := hand ;
         return ;
      end if ;

      if response = "score"
      then
         cmd := score ;
         return ;
      end if ;

      if response = "show"
      then
         cmd := show ;
         return ;
      end if ;

      if response = "help"
      then
         cmd := help ;
         return ;
      end if ;
      
      if Ada.Strings.Fixed.Index(response,"guess ") = 1
      then
         cmd := guess ;
         resp := To_Unbounded_String( response(7..response'last) );
         return ;
      end if ;
      cmd := guess ;
      resp := To_Unbounded_String(response) ;
   end Get ;

end cli;
