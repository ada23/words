with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with Ada.Containers.Indefinite_Multiway_Trees ;
with Ada.Containers.Indefinite_Ordered_Maps ;
with Ada.Containers.Indefinite_Vectors ;

with Interfaces ; use Interfaces ;
package words is
   
  
   verbose : boolean := false ;
   
   function UpCase( lc : string ) return string ;
   
   package DictPkg is new Ada.Containers.Indefinite_Multiway_Trees(string) ;
  
   subtype AnagramsCount is integer range 1..64;
   package WordsPkg is new Ada.Containers.Indefinite_Vectors( AnagramsCount , string ) ;
   procedure ShowWord( c : WordsPkg.Cursor ) ;
   procedure ShowList( l : WordsPkg.Vector ; verbose : boolean := false ) ;

   function EqualVector (Left,Right : WordsPkg.Vector) return boolean ;
   package SigTablePkg is new Ada.Containers.Indefinite_Ordered_Maps(Interfaces.Unsigned_64,WordsPkg.Vector,
                                                                     Interfaces."<" , EqualVector );   
   
   type Dictionary is
      record
         tree : DictPkg.Tree ;
         sigwords : SigTablePkg.Map ;
      end record ;

   
   procedure Load( f : string ; dict : out Dictionary ; minlen : integer := 4; maxlen : integer := 12) ;
   function IsWord( w : string ; dict : Dictionary ) return boolean ;
   procedure Show( dict : Dictionary ) ;
   
   function IsFormedCorrectly( w : string ; h : string ) return boolean ;
   function IsPossibleWord( w : string ; dict : Dictionary ) return boolean ;
   procedure ListAnagrams( dict : Dictionary ; sig : Unsigned_64 ) ;
   procedure ListAnagrams( dict : Dictionary ; w : string ) ;
   function CountAnagrams( dict : Dictionary ; w : string ) return Integer ;
   type BitString is array (integer range <>) of boolean ;
   pragma Pack(BitString) ;
   type Enumerator is
      record
         s : unbounded_string := Null_Unbounded_String ;
         minletters : integer ;
         used : Unsigned_32 := 0 ;
         last : Unsigned_32 := 0;
      end record ;
   function Create( w : string ; min : integer := 3 ) return Enumerator ;
   function Next( e : in out enumerator ) return String ;
   function NoMoreWords( e : enumerator ) return boolean ;
   EndEnumeration : exception ;
   
end words;
