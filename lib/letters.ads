with Interfaces ;
package letters is

   subtype letter is CHARACTER range 'A'..'Z' ;
   
   -- Programming for the Puzzled: Learn to Program While Solving Puzzles (The MIT Press)
   --              Srini Devadas
     
   -- Assign a prime number for each letter The product of the signature of each character
   -- of a word is defined to be the signature of the word. Thus the signature of all anagrams
   -- will be the same.
   Signatures : array (letter'range) of Positive
     := (
         2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 ,
         101
        );   
   function Signature( w : string ;
                      product : boolean := true ) return Interfaces.Unsigned_64 ;

end letters;
