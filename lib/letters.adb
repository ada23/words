with Text_Io; use Text_Io ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Long_Long_Integer_Text_IO ; use Ada.Long_Long_Integer_Text_IO ;
with Ada.Characters.Handling ; use Ada.Characters.Handling ;

package body letters is

   function Signature( w : string ;
                       product : boolean := true ) return Interfaces.Unsigned_64 is
      result : Interfaces.Unsigned_64 := 1 ;
      counted : array(letter'range) of boolean := (others => false);
      use Interfaces ;
      procedure Show (c : character) is
      begin
         Put(c); Put(Long_Long_Integer(result)) ; New_Line ;
      end Show ;    
   begin
      for widx in w'range
      loop
         pragma Debug(Show(w(widx))) ;
         if not counted(To_Upper(w(widx)))
         then
            counted(To_Upper(w(widx))) := true ;
            if product
            then
               result := result * Interfaces.Unsigned_64(Signatures(To_Upper(w(widx)))) ;
            else
               result := result + Interfaces.Unsigned_64(Signatures(To_Upper(w(widx)))) ;
            end if ;
         end if ;
      end loop ;
      return result ;
   exception
      when others =>
         return 0 ;
   end Signature ;
   

end letters;
