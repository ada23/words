with words ;

package scrabble is

   verbosity : integer := 0 ;
   
   type tile is
      record
         face : character ;
         count : integer ;
         score : Integer ;
      end record ;
   
   blank : constant tile := ( ' ' , 2 , 0 ) ;
   subtype Faces is Character range 'A' .. 'Z' ;
   bank : array(Faces'range) of tile := (others => blank) ;
   
   type Bag is array (integer range <>) of tile ;
   type bagptr is access Bag ;
   
   procedure Initialize ;
   function Create return bagptr ;
   procedure Show(t : tile);
   procedure Show(b : bagptr) ;
   

   function Score( w : string ) return Integer ;
   
end scrabble;
