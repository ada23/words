with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Characters.Handling ; use Ada.Characters.Handling ;

package body scrabble is



   procedure Initialize is
   begin
      bank ('A') := ('A', 9, 1);
      bank ('B') := ('B', 2, 3);
      bank ('C') := ('C', 2, 3);
      bank ('D') := ('D', 4, 2);
      bank ('E') := ('E', 12, 1);
      bank ('F') := ('F', 2, 4);
      bank ('G') := ('G', 3, 2);
      bank ('H') := ('H', 2, 4);
      bank ('I') := ('I', 9, 1);
      bank ('J') := ('J', 1, 8);
      bank ('K') := ('K', 1, 5);
      bank ('L') := ('L', 4, 1);
      bank ('M') := ('M', 2, 3);
      bank ('N') := ('N', 6, 1);
      bank ('O') := ('O', 8, 1);
      bank ('P') := ('P', 2, 3);
      bank ('Q') := ('Q', 1, 10);
      bank ('R') := ('R', 6, 1);
      bank ('S') := ('S', 4, 1);
      bank ('T') := ('T', 6, 1);
      bank ('U') := ('U', 4, 1);
      bank ('V') := ('V', 2, 4);
      bank ('W') := ('W', 2, 4);
      bank ('X') := ('X', 1, 8);
      bank ('Y') := ('Y', 2, 4);
      bank ('Z') := ('Z', 1,10);                                                                                                                                                                                                                                                      --  B      2
   end Initialize;

   function Create return bagptr is
      result    : bagptr;
      tilecount : Integer := 2;              -- 2 blank tiles
      bagidx : integer := 0 ;
   begin
      for f in bank'range loop
         tilecount := tilecount + bank (f).count;
      end loop;
      result := new Bag (1 .. tilecount);
      for f in bank'range loop
         for i in 1..bank(f).count
         loop
            bagidx := bagidx + 1;
            result(bagidx) := ( face => f , score => bank(f).score , count => 0 ) ;
         end loop ;
      end loop ;
      --result(bagidx+1) := (face => ' ' , score => 0 , count => 0 );
      --result(bagidx+2) := (face => ' ' , score => 0 , count => 0 );
      return result;
   end Create;

   procedure Show(t : tile) is
   begin
      Put(t.face);
      Set_Col(5);
      Put(t.score);
      Set_Col(18);
      Put(t.count);
      New_Line ;
   end Show ;

   procedure Show(b : bagptr) is
   begin
      for bt in b'range
      loop
         Show(b(bt));
      end loop ;
   end Show ;

   function Score( w : string ) return Integer is
      result : Integer := 0 ;
   begin
      for c in w'range
      loop
         if w(c) /= ' '
         then
            result := result + bank(To_Upper(w(c))).score ;
         end if ;
      end loop ;
      return result ;
   end Score ;
begin
   Initialize ;
end scrabble;
