with Ada.Text_Io ; use Ada.Text_Io;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Strings.Fixed ; use Ada.Strings.Fixed ;
with GNAT.Random_Numbers ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;

with cli ;

package body scrabble.dealer is

   GEN : GNAT.Random_Numbers.Generator;
   vowels : constant String := "AEIOU" ;
   
   function VowelCount( s : string ) return integer is
      result : integer := 0 ;
   begin
      for v in vowels'range
      loop
         result := result + Integer(Ada.Strings.Fixed.Count(s , vowels(v..v))) ;
      end loop ;
      return result ;
   end VowelCount ;
   
   function Deal( b : bagptr ; wl : integer := 7 ) return String is
      result : String( 1..wl ) := ( Others => ASCII.Nul ) ;
      taken : array (b.all'Range) of boolean := ( Others => false ) ;
      ridx : Integer := 0 ;
      nidx : Integer ;
      next : float ;
      vc : integer := 0 ;
   begin
      loop
         vc := 0 ;
         result := (others => ASCII.Nul) ;
         taken := (others => false) ;
         ridx := 0;
         while ridx < wl loop
            next := GNAT.Random_Numbers.Random (GEN) ;
            nidx := Integer(float( b'length ) * next)  ;
            if not taken(nidx)
            then
               ridx := ridx + 1 ;
               result(ridx) := b(nidx).face ;
               taken(nidx) := true ;
            end if ;
         end loop ;
         vc := VowelCount( result ) ;
         if vc >= 1 and vc <= 3
         then
            return result ;
         end if ;
         Put_Line(result & " Regenerate: Vowel Count " & integer'image(vc) );
      end loop ;
      
   end Deal ;
   
   function Possible( g : string ; tileset : string ) return boolean is
      foundc : boolean ;
   begin
      for gc in g'range
      loop
         foundc := false ;
         for tc in tileset'range
         loop
            if g(gc) = tileset(tc)
            then
               foundc := true ;
               exit ;
            end if ;
         end loop ;
         if not foundc
         then
            return false ;
         end if ;
      end loop ;
      return true ;
   end Possible ;
   
   bagp : bagptr ;
   
   procedure Play (dict : words.Dictionary) is
      pw : words.Enumerator ;
      potw : Integer := 0 ;
      hand : string(1..7) ;
      guesses : Words.WordsPkg.Vector ;
   begin
      loop
         declare
            tiles : string := Deal(bagp,7);
         begin
            pw := words.Create(tiles) ;
            while not words.NoMoreWords(pw)
            loop
               potw := potw + words.CountAnagrams(dict , words.Next(pw)) ;
               if potw > wordthreshold
               then
                  hand := tiles ;
                  exit ;
               end if ;
            end loop ;
         end ;
         if potw > wordthreshold
         then
            exit ;
         end if ;
      end loop ;
      Put("Your hand is "); Put(hand); Put(" you have at least "); Put(potw) ; Put(" words you can construct"); New_Line ;
      declare
         cmd : cli.commands ;
         nextword : unbounded_string ;
         score : Integer := 0 ;
         prev : words.WordsPkg.Cursor ;
         use words.WordsPkg ;
      begin   
         loop
            cli.Get("Word",cmd,nextword);
            case cmd is
               when cli.help =>
                   Put_Line("Commands are: hand|score|guess");
                   Put("Your hand is "); Put_Line(hand);
                   
               when cli.quit => 
               	  Put("No of words "); Put(Integer(guesses.Length)) ; 
                  Put(" Score "); Put(score); New_Line ;
                  return ;
               when cli.hand => Put_Line(hand) ;
               when cli.score =>
                  Put(score); New_Line ;
               when cli.show =>
                  words.ShowList(guesses,true ) ;
               when cli.guess =>
                  if not words.IsWord(To_String(nextword),dict)
                  then
                     Put_Line("It is not a word");
                  else
                     if not words.IsFormedCorrectly(To_String(nextword),hand)
                     then
                        Put(To_String(nextword)); Put_Line( " is not formed correctly. Please use only letters dealt");
                     else
                        prev := words.WordsPkg.Find(guesses,To_String(nextword)) ;
                        if prev = words.WordsPkg.No_Element
                        then
                           score := score + scrabble.Score(To_String(nextword));
                           words.WordsPkg.Append(guesses,To_String(nextword)) ;
                        else
                           Put("You have already guessed that word "); Put_Line(To_String(nextword)) ;
                        end if ;
                     end if ;
                  end if ;
               when others => null ;   
            end case ;
         end loop ;
      end ;
      
   end Play ;
   
   procedure Play is
     tiles : string := Deal(bagp,7);
   begin
      Put("Tiles available "); Put_Line(tiles) ;
      loop
         declare
            w : string := cli.Get("Word");
         begin
            if w = "quit"
            then
               exit ;
            end if ;
            if Possible(w , tiles)
            then
               Put("Word Score is "); Put(Score(w)) ; New_Line ;
            else
               Put_Line("Bad guess");
            end if ;
         end ;
      end loop ;
   end Play ;
begin
   GNAT.Random_Numbers.Reset(GEN) ;
   bagp := Create ;
end scrabble.dealer;
