with words ;
package scrabble.dealer is
   wordthreshold : constant := 24 ;
   function Deal( b : bagptr ; wl : integer := 7 ) return String ;
   function Possible( g : string ; tileset : string ) return boolean ;
   procedure Play (dict : words.Dictionary) ;
   procedure Play ;
end scrabble.dealer;
