
with Text_Io; use Text_Io ;
with Ada.Characters.Handling ; use Ada.Characters.Handling ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Long_Long_Integer_Text_IO ; use Ada.Long_Long_Integer_Text_IO ;
with Ada.Strings ;
with Ada.Strings.Fixed ; use Ada.Strings.Fixed;
with letters;
with scrabble ;
package body words is

   function UpCase( lc : string ) return string is
      uc : string := lc ;
   begin
      for c in uc'range
      loop
         uc(c) := To_Upper(uc(c));
      end loop ;
      return uc ;
   end Upcase ;
   
      
   function EqualVector(Left, Right : WordsPkg.Vector ) return boolean is
   begin
      return false ;
   end EqualVector ;
   
   procedure ShowWord( c : WordsPkg.Cursor ) is
   begin
      Put( WordsPkg.Element(c) ) ;
      Set_Col(10) ;
      Put( " score ");
      Put( scrabble.Score(WordsPkg.Element(c)) ) ;
      New_Line;
   end ShowWord ;
   
   procedure ShowList( l : WordsPkg.Vector ; verbose : boolean := false ) is
   begin
      if Verbose
      then
         l.Iterate( ShowWord'access ) ;
      end if ;
   end ShowList ;
   
   procedure Load( f : string ; dict : out Dictionary ; minlen : integer := 4; maxlen : integer := 12) is
      use DictPkg ;
      wf : file_type ;
      word : string(1..64);
      wordlen : natural ;
      d : Dictionary ;
      C : Cursor := Root(d.tree);

      wordcount : integer := 0 ;
      
   begin
      Open(wf,In_File,f) ;
      while Not End_Of_File(wf)
      loop
         Get_Line(wf,word,wordlen) ;
 
         declare
            newword : String := Upcase(Ada.Strings.Fixed.Trim(word(1..wordlen),Ada.Strings.Right)) ;
            newwordsig : Unsigned_64 ;
            wordlist : wordsPkg.Vector ;
         begin
            wordlen := newword'length ;
            if wordlen >= minlen and wordlen <= maxlen
            then
               newwordsig := letters.Signature(newword) ;
               if Verbose
               then
                  Put("Inserting "); Put_Line(newword);
               end if ;
               d.tree.Insert_Child(c,No_Element,newword);

               if verbose
               then
                  Put("Inserting signature "); Put(Long_Long_Integer(newwordsig)); 
                  Put(" for word "); Put(newword);
                  New_Line ;
               end if ;
               if d.sigwords.Contains(newwordsig)
               then
                  if Verbose
                  then
                     Put("Found "); Put(Long_Long_Integer(newwordsig)); Put(" in the dictionary already. adding "); Put(newword); New_Line ;
                  end if ;
                  wordlist := d.sigwords.Element(newwordsig) ;
                  WordsPkg.Append(wordlist,newword) ;
                  d.sigwords.Replace_Element( d.sigwords.Find(newwordsig),wordlist) ;
                  -- d.sigwords.Element(newwordsig).Append(newword) ; 
                  ShowList(wordlist);
               else
                  wordlist.Append(newword) ;
                  d.sigwords.Insert(newwordsig,wordlist) ;
                  if Verbose
                  then
                     Put("Did not find "); Put(Long_Long_Integer(newwordsig)); Put(" starting with "); Put(newword); New_Line ;
                  end if ;
               end if ;    
               wordcount := wordcount + 1 ;
            end if ;
         exception
               when others => null ;
         end ;
      end loop ;
      Close(wf) ;
      Put(wordcount); Put(" words loaded"); New_Line ;
      dict := d ;
   end Load ;
   

   procedure Show( dict : Dictionary ) is
      use DictPkg ;
   begin
      Put("Child Count "); Put( Integer(Node_Count(dict.tree)) ) ; New_Line ;      
   end Show ;
   
   function IsWord( w : string ; dict : Dictionary) return boolean is
      use DictPkg ;
      c : cursor ;
      wu : string := UpCase(Ada.Strings.Fixed.Trim(w,Ada.Strings.Right)) ;
   begin
     
      c := dict.tree.Find( wu );
      if c = No_Element
      then
         Put("Not a word "); Put_Line(w) ;
         return false ;
      end if ;
      Put(w); Put_Line(" is a word");
      return true ;
   end Isword ;
      
   function IsWord2( w : string ; dict : Dictionary) return boolean is
      WA : Integer ;
   begin
      WA := CountAnagrams(dict,w) ;
      if WA > 0
      then
         Put_Line(" Is a word");
         return true ;
      else
         Put_Line(" Is not a word");
         return false ;
      end if ;
   end IsWord2 ;
   
   procedure ListAnagrams( dict : Dictionary ; w : string ) is
      ws : unsigned_64 ;
   begin
      ws := letters.Signature(w) ;
      if Verbose
      then
         Put("ListAnagrams: "); Put(w) ; Put(Long_Long_Integer(ws)); New_Line ;
      end if ;
      ListAnagrams(dict,ws) ;
   end ListAnagrams ;

   function CountAnagrams( dict : Dictionary ; w : string ) return Integer is
      ws : unsigned_64 ;
      wl : WordsPkg.Vector ;
   begin
      ws := letters.Signature(w) ;
      if Verbose
      then   
         Put("ListAnagrams: "); Put(w) ; Put(Long_Long_Integer(ws)); New_Line ;
      end if ;
      if dict.sigwords.Contains(ws)
      then
         wl := dict.sigwords.Element(ws) ;
         return Integer(wl.Length) ;
      else
         return 0 ;
      end if ;
   end CountAnagrams ;
   
   
   procedure ListAnagrams( dict : Dictionary ; sig : Unsigned_64 ) is
      use SigTablePkg ;
      wl : WordsPkg.Vector ;
   begin
      if dict.sigwords.Contains(sig)
      then
         wl := dict.sigwords.Element(sig) ;
         wl.Iterate( ShowWord'access ) ;
      else
         if Verbose
         then   
            Put("Key "); Put(Long_Long_Integer(sig)); Put(" not found"); New_Line ;
         end if ;
      end if ;
   end ListAnagrams ;
   
   function IsFormedCorrectly( w : string ; h : string ) return boolean is
      hu : String := Upcase(h) ;
      wu : String := Upcase(w) ;
   begin
      for c in wu'range
      loop
         if Ada.Strings.Fixed.Index(hu,wu(c..c)) < 1 
         then
            return false ;
         end if ;
      end loop ;
      return true ;
   end IsFormedCorrectly ;
   
   
   function IsPossibleWord( w : string ; dict : Dictionary ) return boolean is
      ws : unsigned_64 ;
   begin
      ws := letters.Signature(w) ;
      if verbose
      then   
         Put("Querying for |"); Put(w); Put("| Signature: ");
         Put(Integer(ws)); New_Line ;
      end if ;

      ListAnagrams(dict,ws) ;
      if Verbose
      then
         declare
            use DictPkg ;
            wc : DictPkg.Cursor ;
         begin
            wc := Dict.Tree.Find(w) ;
            if wc = DictPkg.No_Element
            then
               Put("Did not find the word "); 
            else
               Put("Found the word ");
            end if ;
            Put_Line(w);
         end ;
      end if ;
      return true ;
   end IsPossibleWord;
   
   function Create( w : string ; min : integer := 3 ) return Enumerator is
      result : Enumerator ;
      masklen : Integer := w'length ;
   begin
      result.s := To_Unbounded_String(w) ;
      result.minletters := min ;
      result.used := 0 ;
      result.last := Shift_Left(1 , w'length ) ;
      Put_Line(Integer'Image(Integer(result.last)));
      return result ;
   end Create ;
   
   function Next( e : in out enumerator ) return String is
      use Interfaces ;
      result : string(1..length(e.s)) := (others => ' ');
      ridx : integer := 0 ;
   begin
      e.used := e.used + 1 ;
      --  Put(Integer'Image(integer(e.used)));
      --  Put(Integer'Image(integer(e.last)));

      if e.used >= e.last
      then
         raise EndEnumeration ;
      end if ;
      for c in 1..length(e.s)
      loop
         if (Shift_Left(1,c-1) and e.used) /= 0 
         then
            ridx := ridx + 1 ;
            result(ridx) := Element(e.s , c ) ;
         end if ;
      end loop ;
      --  Put(Integer'Image(integer(e.used)));
      --  Put(Integer'Image(integer(e.last)));
      --  New_Line;
      --  Flush;
      
      return Trim(result,Ada.Strings.right );
   end Next ;
   function NoMoreWords( e : enumerator ) return boolean is
      nextnum : Unsigned_32 := e.used + 1 ;
   begin
      if nextnum >= e.last
      then
         return true ;
      end if ;
      return false ;
   end NoMoreWords ;
end words;
