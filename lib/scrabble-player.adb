with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;

with cli ;

package body scrabble.player is
      
   procedure Play (dict : words.Dictionary) is
      use cli ;
      cmd : cli.commands := cli.undefined ;
      hand : unbounded_string ;
   begin
      while cmd /= cli.quit
      loop
         cli.Get("Hand",cmd,hand);
         if cmd = cli.quit
         then
            return ;
         end if ;
         if cmd = cli.guess
         then
            Play( dict , To_String(hand) ) ;
            return ;
         end if ;
      end loop ;
   end Play ;
   
   procedure Play (dict : words.Dictionary ; hand : String) is
      e : words.Enumerator ;
   begin
      e := words.Create(hand) ;
      while not words.NoMoreWords(e)
      loop
         declare
            w : string := words.Next(e) ;
         begin
            words.ListAnagrams(dict,w);
         end ;
      end loop ;
   end Play ;
   
end scrabble.player;
